$(document).ready(function(){

    $('#travelTypeResult').html(GetURLParameter('travel_type'));
    $('#departureResult').html(GetURLParameter('departure'));               
    $('#arrivalResult').html(GetURLParameter('arrival')); 
    $('#departureDateResult').html(GetURLParameter('departureDate'));
    $('#arrivalDateResult').html(GetURLParameter('arrivalDate'));
    $('#passengersResult').html(GetURLParameter('passengers'));

});

function GetURLParameter(sParam){
    var parameters = sessionStorage.getItem("url_values"); 
    var sURLVariables = parameters.split('&');
    for (var i = 0; i < sURLVariables.length; i++){
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam){
            return sParameterName[1] === undefined ? 'No seleccionado' : decodeURIComponent(sParameterName[1]);
        }
    }
}