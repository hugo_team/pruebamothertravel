$(document).ready(function(){    

    sessionStorage.setItem("url_values", '' );

    $('.autocomplete').on('click, keyup',function(){        
        var event = $(this);
        $.ajax(
        'https://api.myjson.com/bins/17v3ly'
        ).done(function(data) {        
            var availableTags = [];
            data.aeropuertos.forEach(function(aeropuerto){            
                availableTags.push(aeropuerto.name);
            });
            event.autocomplete({
                source: availableTags
            });
        }).fail(function(jqXHR) {
            console.log(jqXHR.statusText);
        });
    });


    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd-mm-yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
        };
        
        $.datepicker.setDefaults($.datepicker.regional['es']);
  
    $('.datepicker').datepicker({    
        forceParse: false,
        autoclose: true,
        todayHighlight: true,
        toggleActive: true,
        minDate: 0,
        startDate: 0,
    });
    
    $("input[name=travel_type]").click(function(){
        console.log($("input[name=travel_type]:checked").val());
    });

    $( "#saveNewTravel" ).click( function( event ) {
        event.preventDefault();
        sessionStorage.setItem("url_values", $('#travelFrm').serialize() );
        window.location.href = "resultados.html";

      });
});


